# -*- coding: utf-8 -*-

import asyncio
from motor.motor_asyncio import AsyncIOMotorClient
from bson.objectid import ObjectId
import datetime, time
import hashlib

loop = asyncio.get_event_loop()

MONGO_HOST = '192.168.0.235'
MONGO_DB = 'site_crawler'
MONGO_URI = 'mongodb://{}/{}'.format(MONGO_HOST, MONGO_DB)
MONGO_API_TASK_COLLECTION = 'request'

client = AsyncIOMotorClient(MONGO_URI)
db = client.get_default_database()
coll = db[MONGO_API_TASK_COLLECTION]
#
# async def drop():
#     db.request.drop()

# db.getCollection('request').find({ $ and: [{"url": / baby.ru /}, {"site_data": {$ne:null}}]})

async def get():
#     item = await coll.find_one({"url" : "12254555a1fbdae18559b46ca7f94151"})
    item = await coll.find_one({ "$and": [{ "$and": [{"url": "/baby.ru/"}, {"url": "/40155/"}]}, {"site_data": {"$ne": ""}}]})

    print(item)
# .find()

# loop.run_until_complete(drop())
loop.run_until_complete(get())


    # async def write():
#     now = datetime.datetime.now().timestamp()
#     await coll.insert({'text': hashlib.md5(str(now).encode()).hexdigest()})
#
# tasks = []
# for i in range(1000000):
#     tasks.append(write())
#
# loop.run_until_complete(asyncio.gather(*tasks))

# async def get():
#     item = await coll.find_one({"url" : "12254555a1fbdae18559b46ca7f94151"})
# now = time.time()
# loop.run_until_complete(get())
# print(time.time() - now)
# #0.006390094757080078