import jwt
from settings import JWT_ALGORITHM, JWT_SECRET


jwt_token = jwt.encode({"user": "debug"}, JWT_SECRET, JWT_ALGORITHM)
print(jwt_token.decode('utf-8'))
