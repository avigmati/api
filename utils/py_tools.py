import os
import time
from lxml import etree
from urllib import parse
import http.cookiejar as cookielib
import string
import base64

from grab import Grab


def to_cookielib_cookie(selenium_cookie):
    return cookielib.Cookie(
        version=0,
        name=selenium_cookie['name'],
        value=selenium_cookie['value'],
        port='80',
        port_specified=False,
        domain=selenium_cookie['domain'],
        domain_specified=True,
        domain_initial_dot=False,
        path=selenium_cookie['path'],
        path_specified=True,
        secure=selenium_cookie['secure'],
        expires=selenium_cookie['expiry'],
        discard=False,
        comment=None,
        comment_url=None,
        rest=None,
        rfc2109=False
    )


def put_cookies_in_jar(selenium_cookies):
    cookie_jar = cookielib.CookieJar()
    for cookie in selenium_cookies:
        cookie_jar.set_cookie(to_cookielib_cookie(cookie))
    return cookie_jar


def get_month_count_in_string(string):
    """
    Преобразование строки вида "Опыт работы 2 года 3 месяца" или "Experience 2 years 3 month" в кол-во месяцев
    :param string:
    :return:
    """
    year = 0
    month = 0
    number = str()
    is_digit = False
    for c in string:
        if c.isdigit():
            is_digit = True
            number += c
        elif c.isalpha():
            if is_digit:
                if c.lower() in ['y', 'г', 'л']:
                    year = int(number)
                    number = str()
                elif c.lower() in ['m', 'м']:
                    month = int(number)
                    number = str()
                is_digit = False
    return int(month + year * 12)


def normalize_space(string):
    """
    Заменяет все непечатные символы одним пробелом и удаляет повоторяющиеся пробелы
    :param string:
    :return: str:
    """
    result = str()
    was_space = False
    for ch in string:
        if ch.isprintable() and ch != ' ':
            result += ch
            was_space = False
        else:
            if not was_space:
                was_space = True
                result += ' '

    return result.strip()


def period_to_date(string):
    """
    Преобразует строку с периодом вида "Марта 2015 - Июль 2015" в 4 переменных
    :param string:
    :return:
    """
    months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
              'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    string = string.replace('\xa0', ' ')
    month_start = year_start = month_end = 0
    # Если период имеет начало и конец
    if string.find('—') != -1:
        period = string.strip().split('—')
        if period[0].strip().find(' ') != -1:
            p0 = period[0].strip()
            month_start, year_start = p0.split(' ')
            try:
                month_start = months.index(month_start.strip()) + 1
            except:
                try:
                    month_start = time.strptime(month_start.strip(), '%B').tm_mon
                except:
                    month_start = 0

        else:
            year_start = period[0].strip()

        if period[1].strip().find(' ') != -1:
            p1 = period[1].strip().split(' ')
            if len(p1) > 1 and not p1[1].isdigit():
                month_end = year_end = 0
            else:
                month_end, year_end = p1
                try:
                    month_end = months.index(month_end.strip()) + 1
                except:
                    try:
                        month_end = time.strptime(month_end, '%B').tm_mon
                    except:
                        month_end = 0
        else:
            if period[1].strip().isdigit():
                year_end = period[1].strip()
            else:
                year_end = 0
    else:
        if string.strip().find(' ') != -1:
            s = string.strip()
            month_end, year_end = s.split(' ')
            # Если работает по настоящее время
            if not year_end.isdigit():
                year_end = month_end = 0
            else:
                try:
                    month_end = months.index(month_end.strip()) + 1
                except:
                    try:
                        month_end = time.strptime(month_end, '%B').tm_mon
                    except:
                        month_end = 0
                year_end.strip()
        else:
            if string.isdigit():
                year_end = string.strip()
            else:
                year_end = 0

    if month_start == 0 and year_start == 0:
        begin = '0000-00-00 00:00:00'
    else:
        begin = '{}-{}-01 00:00:00'.format(str(year_start).zfill(4),
                                           str(month_start if month_start > 0 else 1).zfill(2))

    if month_end == 0 and year_end == 0:
        end = '0000-00-00 00:00:00'
    else:
        end = '{}-{}-01 00:00:00'.format(str(year_end).zfill(4), str(month_end if month_end > 0 else 1).zfill(2))

    return end, begin


def get_filename_from_get(get):
    f_name = list(parse.urlsplit(get))[2].split('/')[-1]
    return f_name


def get_update_datetime(string):
    months = ['янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']
    arr = string.split()
    if len(arr) == 4:
        month = str(int(months.index(arr[2][:3])+1))
        year = time.gmtime().tm_year
        tm = arr[3]
        day = arr[1]
        return '{}-{}-{} {}:00'.format(year, month.zfill(2), day.zfill(2), tm)
    elif len(arr) == 5:
        month = str(int(months.index(arr[2][:3])+1))
        year = arr[3].split(',')[0]
        tm = arr[4]
        day = arr[1]
        return '{}-{}-{} {}:00'.format(year, month.zfill(2), day.zfill(2), tm)
    else:
        return string


def el_to_grab(el, pretty_print=False):
    return Grab(etree.tostring(el, encoding='UTF-8', pretty_print=pretty_print))


def get_node_list(g, xpath):
    try:
        node = g.doc.select(xpath).node_list()
        return node
    except:
        return None


def get_node(g, xpath):
    try:
        node = g.doc.select(xpath).node()
        if type(node) is str:
            node.strip().replace('\xa0', ' ')
        return node
    except:
        return None


def get_node_text(g, xpath):
    try:
        node = g.doc.select(xpath).node().text_content()
        if node is not None:
            node = ' '.join(str(node).split())
        return node
    except:
        return None


def google_region_appendix(cname):
    """
    возвращает аппендикс к url google search, в которой закодирован регион поиска. источник: http://fuget.ru/post/84
    cname регионов: https://developers.google.com/adwords/api/docs/appendix/geotargeting
    :param cname:
    :return:
    """
    def ckey(cname):
        keys = list(string.ascii_uppercase) + list(string.ascii_lowercase) + list(range(0, 10)) + ['-', ' ']
        return keys[len(cname) % len(keys)]
    return '&uule=w+CAIQICI{}{}'.format(ckey(cname), base64.b64encode(bytes(cname.encode('ascii'))).decode())


def save_html(driver, BASEDIR):
    html_dir = os.path.join(BASEDIR, 'htmls')
    if not os.path.exists(html_dir):
        os.makedirs(html_dir)
    file = os.path.join(html_dir, ''.join([time.strftime('%Y-%m-%d_%H%M%S'), '.html']))
    with open(file, 'wb') as f:
        f.write(driver.page_source.encode('utf-8', 'ignore'))
