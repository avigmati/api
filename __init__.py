# -*- coding: utf-8 -*-

__version_info__ = ('0', '1')
__version__ = '.'.join(__version_info__)

__api_version__ = '1'
