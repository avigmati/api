# -*- coding: utf-8 -*-

import asyncio
import logging

import settings
from core import init, stop, setup_logging

setup_logging()
logger = logging.getLogger(__name__)

loop = asyncio.get_event_loop()

async def run():
    app = await init()
    handler = app.make_handler()
    srv = await loop.create_server(handler, settings.SERVER_HOST, settings.SERVER_PORT)
    logger.info("[API server] started at http://{}:{}".format(settings.SERVER_HOST, settings.SERVER_PORT))
    return srv, handler, app

if __name__ == '__main__':
    srv, handler, app = loop.run_until_complete(run())
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        stop(srv, handler, app)
    loop.close()

# https://github.com/kashifrazzaqui/vyked/blob/develop/examples/accountservice.py

#  try\exc
# def _as_future(fun, *args, **kwargs):
#     try:
#         res = fun(*args, **kwargs)
#     except Exception as e:
#         f = Future()
#         f.set_exception(e)
#         return f
#     else:
#         if isinstance(res, Future):
#             return res
#         elif iscoroutine(res):
#             return asyncio.Task(res)
#         else:
#             f = Future()
#             f.set_result(res)
#             return f
