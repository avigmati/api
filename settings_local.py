# -*- coding: utf-8 -*-

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8001

DEBUG = True

LOG_CONFIG = 'logging_dev.yaml'

JWT_SECRET = "ljhsdY*&HdDQSDdf!54365e5xsFDHG&^$$--bWUEsdz11e*&^fIHDW78E23*&%*&^rtfgtf%^$%$^&SZFD"

MONGO_HOST = 'localhost'
MONGO_DB = 'api'
MONGO_URI = 'mongodb://{}'.format(MONGO_HOST)
MONGO_API_TASK_COLLECTION = 'task'
MONGO_USER_COLLECTION = 'user'

RABBIT_HOST = 'localhost'
RABBIT_PORT = 5672
RABBIT_USER = 'guest'
RABBIT_PASSWORD = 'guest'

BOTS = [
    # 'apps.bots.yandex_xml_bot',
    # 'apps.bots.site_crawler_bot',
    # 'apps.bots.yandex_search2_bot',
    # 'apps.bots.google_search2_bot',
    # 'apps.bots.wordstat2_bot',
    # 'apps.bots.headhunter_bot'
]


APIS = [
    # 'apps.apis.yandex_xml',
    # 'apps.apis.yandex_direct',
    # 'apps.apis.yandex_search2',
    # 'apps.apis.google_search2',
    # 'apps.apis.wordstat2',
    # 'apps.apis.headhunter'
    'apps.apis.simply'
]


SERVICES = [
    # 'apps.services.fetcher'
]
