# -*- coding: utf-8 -*-

import asyncio
from aiohttp import web
from motor.motor_asyncio import AsyncIOMotorClient

import traceback
import importlib
import logging

import __init__
import settings

from . import setup_logging, auth_middleware

setup_logging()
logger = logging.getLogger(__name__)

loop = asyncio.get_event_loop()

display = None
if settings.USE_DISPLAY:
    from pyvirtualdisplay import Display
    # global display
    # display = Display(visible=0, size=(1920, 1080))
    # display.start()

async def init():
    app = web.Application(loop=loop, middlewares=[auth_middleware])
    app.settings = settings

    logger.info('[API Server] v{}, API version {}, starting...'.format(__init__.__version__, settings.API_VERSION))
    logger.info('')

    logger.info('[API server] Create connection to MongoDB [{}]...'.format(settings.MONGO_URI))
    app.mongo_client = AsyncIOMotorClient(settings.MONGO_URI, max_pool_size=1)
    app.mongo_db_api = app.mongo_client[settings.MONGO_DB]
    app.api_task_collection = app.mongo_db_api[app.settings.MONGO_API_TASK_COLLECTION]
    app.user_collection = app.mongo_db_api[app.settings.MONGO_USER_COLLECTION]
    logger.info('[API server] User collection [{}]'.format(app.settings.MONGO_USER_COLLECTION))
    logger.info('[API server] Task collection [{}]'.format(app.settings.MONGO_API_TASK_COLLECTION))
    if settings.USE_DISPLAY:
        global display
        display = Display(visible=0, size=(1920, 1080))
        display.start()

    logger.info('')

    app.bots, app.apis, app.services = [], [], []
    for m in settings.BOTS + settings.SERVICES + settings.APIS:
        try:
            module = importlib.import_module(m)
            cls = getattr(module, getattr(module, 'TYPE'))
            logger.info('[{}] Initializing...'.format(getattr(module, 'NAME')))
            instance = cls(app)
            await instance.initialize()
            if getattr(module, 'TYPE') == 'API':
                logger.info('[{}] Route: {} {}'.format(instance.get_name(), instance.get_request_type(),
                                                       instance.get_route()))
                app.router.add_route('*', instance.get_route() + '{tail:.*}', instance.handler)
                app.apis.append(instance)
            elif getattr(module, 'TYPE') == 'BOT':
                app.bots.append(instance)
            elif getattr(module, 'TYPE') == 'SERVICE':
                app.services.append(instance)
            logger.info('')
        except Exception as e:
            logger.error(str(e))
            logger.error(traceback.format_exc())
            settings.sys.exit(2)
    return app


def stop(srv, handler, app):
    logger.info('')
    logger.info('[API server] stopping...')
    if display:
        display.stop()

    for api in app.apis:
        logger.info('[{}] closing...'.format(api.get_name()))
        loop.run_until_complete(api.close())
    for bot in app.bots:
        logger.info('[{}] closing...'.format(bot.get_name()))
        loop.run_until_complete(bot.close())
    loop.run_until_complete(handler.finish_connections(60.0))
    srv.close()
    loop.run_until_complete(srv.wait_closed())
    loop.run_until_complete(app.finish())
    for task in asyncio.Task.all_tasks():
        task.cancel()
    logger.info('[API server] stopped')
