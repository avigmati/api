# -*- coding: utf-8 -*-

import asyncio
import aiohttp
import aioamqp
from aiozmq import rpc

from aiohttp import web
import requests
from urllib.parse import urlparse

from motor.motor_asyncio import AsyncIOMotorClient
from bson.json_util import loads, dumps

from functools import partial
import datetime
import logging
import traceback

from core import setup_logging
from core.routes import HTTPMethodNotAllowed, HTTPNotFound
from core.methods import MethodNotFound

setup_logging()
logger = logging.getLogger(__name__)

loop = asyncio.get_event_loop()

MAX_REQUESTS = 30

HTTP_TIMEOUT = 30

HEADERS = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'user-agent:': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                   'Chrome/43.0.2357.81 Safari/537.36',
    # 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    # 'Accept-Encoding': 'gzip, deflate, sdch',
    # 'Accept-Language': 'ru-RU,ru;q=0.8',
    # 'content-type': 'text/plain'
    # 'User-Agent': 'python-requests/2.9.1',
    # 'Accept': '*/*',
    # 'Accept-Encoding': 'gzip, deflate',
    # 'Connection': 'keep-alive',
}


class RequestException(Exception):
    def __init__(self, message, code, traceback, start=None, total=None):
        self.message = 'Response error message: {}'.format(message)
        self.code = code
        self.traceback = traceback
        self.start = start
        self.total = total


class Base(object):

    STATUS = {}

    def __init__(self, app, settings, *args, **kwargs):
        self.app = app
        self.settings = settings
        self.routes = None

    def get_name(self):
        raise Exception('Must implement get_name().')

    async def get_status(self):
        raise Exception('Must implement get_status().')

    async def close(self):
        raise Exception('Must implement close().')

    async def initialize(self, *args, **kwargs):
        pass

    async def handler(self, request):
        pass

    def decode_data(self, raw_data):
        try:
            data = raw_data.decode('utf-8')
            return data
        except Exception as e:
            pass
        try:
            data = raw_data.decode('cp1251')
            return data
        except Exception as e:
            pass
        return None

    async def get(self, url, post_data, method='GET', connector=None, time_it=False, last_request=None):
        # throttle
        # if last_request:
        #     if last_request.get('domain') and last_request.get('start'):
        #         if last_request['domain'] == urlparse(url).netloc:
        #             if (datetime.datetime.now() - last_request['start']).total_seconds() < 1:
        #                 await asyncio.sleep(1)

        if connector:
            session = aiohttp.ClientSession(headers=self.headers, connector=connector)
        else:
            session = aiohttp.ClientSession(headers=self.headers)
        with session:
            with aiohttp.Timeout(self.http_timeout):
                try:
                    start_r, total = None, None
                    if time_it:
                        start_r = datetime.datetime.now()

                    data = None
                    if method.lower() == 'get':
                        response = await session.request('get', url)
                    else:
                        response = await session.request('post', url, data=post_data)
                    if response:
                        if response.status == 200:
                            data = await response.read()
                            await response.release()
                            if time_it:
                                total = (datetime.datetime.now() - start_r).total_seconds()
                        else:
                            await response.release()
                            if time_it:
                                total = (datetime.datetime.now() - start_r).total_seconds()
                            raise RequestException(response.reason, response.status, '', start=start_r, total=total)

                    if time_it:
                        return data, start_r, total
                    else:
                        return data

                except RequestException as e:
                    raise

                except Exception as e:
                    raise e


class BaseAPI(Base):

    def __init__(self, app, settings, *args, **kwargs):
        super(BaseAPI, self).__init__(app, settings, *args, **kwargs)
        self.routes = None

        if kwargs.get('semaphore'):
            self.semaphore = kwargs.get('semaphore')
        else:
            max_requests = self.settings.MAX_REQUESTS if hasattr(self.settings, 'MAX_REQUESTS') else MAX_REQUESTS
            self.semaphore = asyncio.Semaphore(kwargs.get('max_requests', max_requests))
        self.headers = self.settings.HEADERS if hasattr(self.settings, 'HEADERS') else HEADERS
        self.http_timeout = self.settings.HTTP_TIMEOUT if hasattr(self.settings, 'HTTP_TIMEOUT') else HTTP_TIMEOUT

    async def add_routes(self, routes):
        self.routes = routes

    def get_route_from(self, request):
        path = request.path
        path = path + '/' if not path.endswith('/') else path
        # base_path = self.get_route() + '/' if not self.get_route().endswith('/') else self.get_route()
        base_path = self.get_route()
        return path.split(base_path)[1]

    async def handler(self, request):
        if not self.routes:
            raise Exception('Must add routes.')
        try:
            response = await self.routes.call(self.get_route_from(request), request.method, request, self)
            return response
        except HTTPMethodNotAllowed as e:
            return aiohttp.web.HTTPMethodNotAllowed(method=e.method, allowed_methods=e.allowed_methods)
        except HTTPNotFound:
            return aiohttp.web.HTTPNotFound()

    def get_request_type(self):
        return 'GET'

    def get_route(self):
        raise Exception('Must implement get_route().')

    async def create_api_task(self, request):
        params = await self.validator.valid_params(request)
        params['api'] = self.get_name()
        params['mod_date'] = datetime.datetime.now()
        task_id = await self.app.api_task_collection.insert(params)
        task = await self.app.api_task_collection.find_one({'_id': task_id})
        logger.info('[{}] [{}] : Task created'.format(self.get_name(), str(task_id)))
        return task


class BaseSERVICE(rpc.AttrHandler, Base):

    def __init__(self, app, settings, *args, **kwargs):
        super(BaseSERVICE, self).__init__(app, settings, *args, **kwargs)
        self.methods = None

    async def add_methods(self, methods):
        self.methods = methods
        return methods

    def get_methods_names(self):
        return list(self.methods.methods.keys())

    @rpc.method
    async def exec(self, method, request):
        response = None
        try:
            response = await self.methods.call(method, request, self)
        except Exception as e:
            logger.exception('[{}] [{}] [{}]'.format(self.get_name(), method, request))
        return response


class ServiceConnector(object):

    def __init__(self, sock, params):
        self.sock = sock
        self.params = params
        self.rpc_client = None

    async def initialize(self):
        logger.info('Connect to [{}]...'.format(self.sock))
        self.rpc_client = await rpc.connect_rpc(connect=self.sock)
        if self.rpc_client:
            await self.rpc_client.call.exec('setup_client', self.params)

    async def exec(self, method, data):
        return (await self.rpc_client.call.exec(method, data))


class BaseServiceWorker(Base):

    def __init__(self, app, settings, *args, **kwargs):
        super(BaseServiceWorker, self).__init__(app, settings, *args, **kwargs)


class MongoRequestMixin(object):

    mongo_client_request = None
    mongo_db_request = None
    request_collection = None

    def __init__(self, *args, **kwargs):
        super(MongoRequestMixin, self).__init__(*args, **kwargs)

        logger.info('[{}] Create connection to MongoDB [{}]...'.format(self.get_name(), self.settings.MONGO_URI))
        self.mongo_client_request = AsyncIOMotorClient(self.settings.MONGO_URI, max_pool_size=1)
        self.mongo_db_request = self.mongo_client_request.get_default_database()
        self.request_collection = self.mongo_db_request[self.settings.MONGO_REQUEST_COLLECTION]

    async def get_scanned(self, request_data, period='week', additional_params=None):
        if period == 'week':
            now = datetime.datetime.now()
            start = now - datetime.timedelta(days=now.weekday())
            scan_week_from = start.replace(hour=00, minute=00, second=00)
            end = start + datetime.timedelta(days=6)
            scan_week_to = end.replace(hour=23, minute=59, second=59)

            search_params = {
                'request': request_data['request'],
                'date': {'$gte': scan_week_from, '$lte': scan_week_to}
            }
            if additional_params:
                search_params.update(additional_params)

            scanned_request = await self.request_collection.find_one(search_params)
            return scanned_request

    async def create_request(self, request_data):
        request_data = await self.validator.valid_params(request_data)

        additional_params = None
        if request_data['from'] == 'Wordstat 2 API':
            additional_params = {'column': request_data['column']}

        scanned_request = await self.get_scanned(request_data, additional_params=additional_params)
        scanned = False

        if scanned_request:
            request = scanned_request
            if not request['status'] == 'complete' or request_data['force_fetch']:
                if not request['status'] == 'complete':
                    logger.warning('[{}] [{}] : Request scanned at week: [{}], but not complete: [{}], '
                                   'current request task: [{}]'
                                   .format(self.get_name(), str(request['_id']), request['request'], request['status'],
                                           request_data['request_task']))

                elif request_data['force_fetch']:
                    logger.warning('[{}] [{}] : Request scanned at week: [{}] status: [{}], but task requested rescan, '
                                   'current request task: [{}]'
                                   .format(self.get_name(), str(request['_id']), request['request'], request['status'],
                                           request_data['request_task']))
                else:
                    pass

                _id = request['_id']
                orig_request_task = request['request_task']
                del request['_id']
                request.update(request_data)

                request.update({'request_task': orig_request_task, 'mod_date': datetime.datetime.now()})

                # # Todo: HARDCODE FOR YANDEX_XML_BOT !!!!!!!!!!!
                # if request["from"] == "Yandex Xml API":
                #     status = 'process'
                #     data = None
                #     if request['status'] == 'fetching sites':
                #         status = 'fetching sites'
                #         data = request['data']
                #     if request['status'] == 'complete':
                #         status = 'fetching sites'
                #         data = request['data']
                #
                #     request.update({'request_task': orig_request_task, 'data': data, 'status': status,
                #                     'mod_date': datetime.datetime.now()})
                #
                # # Todo: HARDCODE FOR SITE_CRAWLER_BOT !!!!!!!!!!!
                # if request["from"] == "Yandex Xml BOT":
                #     status = 'process'
                #     data = request['data']
                #     request.update({'request_task': orig_request_task, 'data': data, 'status': status,
                #                     'mod_date': datetime.datetime.now()})
                # else:
                #     request.update({'request_task': orig_request_task, 'data': None, 'status': 'process',
                #                     'mod_date': datetime.datetime.now()})

                await self.request_collection.update(
                    {'_id': _id},
                    {'$set': request}
                )
                request = await self.request_collection.find_one({'_id': _id})
            else:
                scanned = True
                logger.warning('[{}] [{}] : Request scanned at week: [{}], current request task: [{}]'
                               .format(self.get_name(), str(request['_id']), request['request'], request_data['request_task']))
        else:
            request_data.update({'data': None, 'status': 'process', 'message': None,
                                 'date': datetime.datetime.now()})
            request = await self.request_collection.insert(request_data)
            request = await self.request_collection.find_one({'_id': request})
            logger.info('[{}] [{}] : Request created: [{}], from api task: [{}]'
                        .format(self.get_name(), str(request['_id']), request['request'], request['request_task']))
        return request, scanned


def rabbit_reconnect(method):
    async def wrapped(self, *args, **kwargs):
        if not self.rabbit_protocol:
            await self.rabbit_connect()
        return await method(self, *args, **kwargs)
    return wrapped


class Rabbit:

    app = None
    api = None
    rabbit_protocol = None
    rabbit_transport = None

    def __init__(self, app, api):
        self.app = app
        self.api = api

    async def initialize(self):
        await self.rabbit_connect()

    async def rabbit_connect(self):
        logger.info('[{}] Create connection to RabbitMQ [{}@{}:{}]...'.format(self.api.get_name(),
                                                                              self.app.settings.RABBIT_USER,
                                                                              self.app.settings.RABBIT_HOST,
                                                                              self.app.settings.RABBIT_PORT))
        self.rabbit_transport, self.rabbit_protocol = await aioamqp.connect(self.app.settings.RABBIT_HOST,
                                                                            self.app.settings.RABBIT_PORT,
                                                                            login=self.app.settings.RABBIT_USER,
                                                                            password=self.app.settings.RABBIT_PASSWORD)

    async def close(self):
        if self.rabbit_protocol:
            self.rabbit_protocol.close()
        if self.rabbit_transport:
            self.rabbit_transport.close()


class Celery(Rabbit):

    channel = None

    def __init__(self, app, api):
        super(Celery, self).__init__(app, api)

    async def initialize(self):
        await super(Celery, self).initialize()
        logger.info('[{}] Celery status exchange is [{}]'.format(self.api.get_name(),
                                                                 self.api.settings.CELERY_STATUS_EXCHANGE))

    @rabbit_reconnect
    async def send(self, message):
        self.channel = await self.rabbit_protocol.channel()

        _message = self.api.settings.CELERY_MESSAGE
        _message['id'] = message['id']
        _message['kwargs']['task'] = message['id']
        _message['kwargs']['status'] = message['status']

        await self.channel.basic_publish(dumps(_message),
                                         exchange_name=self.api.settings.CELERY_STATUS_EXCHANGE,
                                         routing_key='',
                                         properties={'content_type': 'application/json'})

