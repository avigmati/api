# -*- coding: utf-8 -*-
import base64
import datetime
import time
from abc import ABCMeta, abstractmethod
from queue import Queue
import traceback
import logging
from configparser import ConfigParser

from selenium import webdriver
from pyvirtualdisplay import Display

from settings import *

from core.log import setup_logging

setup_logging()
logger = logging.getLogger(__name__)

display = None
API_ROOT_DIR = os.path.dirname(os.path.dirname(__file__))


class BaseSyncAPI(object):
    __metaclass__ = ABCMeta

    # @abstractmethod
    def get_name(self):
        return self.__class__.__name__
        # raise Exception('Must implement get_name().')

    # @abstractmethod
    def close(self):
        raise Exception('Must implement close().')

    @abstractmethod
    def run_task(self):
        raise Exception('Must implement run_task().')


class SeleniumMixin(object):
    __metaclass__ = ABCMeta

    proxy_list_queue = Queue()
    driver_queue = Queue()
    user_list_queue = Queue()

    request = None

    proxy_host = None
    proxy_port = None

    driver = None
    driver_used = 0
    use_display = False

    @staticmethod
    def serialize_queues(class_name):
        c = class_name
        return {
            'proxy_list_queue': c.queue_to_list(c.proxy_list_queue),
            'driver_queue': c.queue_to_list(c.driver_queue),
            'user_list_queue': c.queue_to_list(c.user_list_queue)
        }

    @staticmethod
    def deserialize_queues(class_name, s):
        c = class_name
        c.proxy_list_queue = c.list_to_queue(s.get('proxy_list_queue'))
        c.driver_queue = c.list_to_queue(s.get('driver_queue'))
        c.user_list_queue = c.list_to_queue(s.get('user_list_queue'))

    @staticmethod
    def queue_to_list(q):
        l = []
        while not q.empty():
            l.append(q.get())
        return l

    @staticmethod
    def list_to_queue(l):
        q = Queue()
        for item in l:
            q.put(item)
            q.task_done()
        return q

    def __init__(self):
        self._user_config = {}
        self.proxy_requests = 0
        self.proxy_has_auth = False
        self.proxy_user = None
        self.proxy_password = None
        self.blocked_until = None
        self.user_list_file = None
        self.captcha_sequence = 0
        self.user_login = None
        self.user_password = None
        self.is_logged_in = False
        self.proxy_list_file = None
        self.user_config_file = None
        self.browser_user_agent = None
        self.max_requests_in_per_user_connection = 1
        self.user_list_file_login_password_separator = None

    def get_task_id(self):
        if self.request['from'] == 'hh':
            _id = self.request.get('request_task', None)
        else:
            _id = str(self.request['_id'])
        return _id

    def get_data(self):
        result = []

        try:
            global display
            if self.use_display:
                display = Display(visible=0, size=(1024, 768))
                display.start()

            self.open_browser()

            req_result = self.load_request_result()

            if req_result:
                result = self.parse_html_list()
            self.close_browser(True)

            if display:
                 display.stop()

            return result

        except Exception as e:
            logger.error(traceback.format_exc())
            self.close_browser(True)
            if display:
                display.stop()

    def fill_proxy_queue(self):
        """
        Заполняет очередь прокси, для работы с ними сразу во множестве потоков
        :return:
        """
        if not getattr(self, 'proxy_use', False) or \
                not self.__class__.proxy_list_queue.empty() or \
                not os.path.exists(getattr(self, 'proxy_list_file', None)):
            self.proxy_host, self.proxy_port = None, None
            return False
        proxy_count = 0
        with open(self.proxy_list_file, 'r', encoding="UTF-8") as f:
            for line in f:
                if line.startswith('#') or len(line.strip()) == 0:
                    continue
                self.__class__.proxy_list_queue.put(line.strip())
                proxy_count += 1
        if not getattr(self, 'allow_no_proxy', True) and proxy_count == 0:
            raise Exception("Proxy list is empty!")
        return True

    def install_proxy_from_queue(self, force=False):
        if self.__class__.proxy_list_queue.empty():
            self.proxy_host, self.proxy_port = None, None
            return False

        if self.proxy_host is None or self.proxy_requests >= getattr(self, 'max_requests_in_per_connection', 0) \
                or force:
            proxy = self.__class__.proxy_list_queue.get()
            # Прокси имеет данные авторизаци
            if proxy.find('@') != -1:
                auth, srv = proxy.split('@')
                self.proxy_host, self.proxy_port = srv.split(':')
                self.proxy_user, self.proxy_password = auth.split(':')
                self.proxy_has_auth = True
            else:
                self.proxy_has_auth = False
                self.proxy_host, self.proxy_port = proxy.split(':')

            if getattr(self, 'reuse_proxy_is_allowed', True):
                self.__class__.proxy_list_queue.put(proxy)
                self.__class__.proxy_list_queue.task_done()

            self.proxy_requests = 0

            logger.info("[{}] [{}] : Proxy changed to {}:{}.".format(self.get_name(), self.get_task_id(),
                                                                     self.proxy_host, self.proxy_port))
        else:
            self.proxy_requests += 1

    # noinspection PyBroadException,PyUnusedLocal
    def check_driver_alive(self):
        try:
            handle = self.driver.current_window_handle
            return True
        except:
            return False

    def open_browser(self, dedicated_proxy=False, allow_no_proxy=True):
        """
        :param dedicated_proxy: когда необходимо использовать выделенный на 1 пользователя прокси-сервер
        :param allow_no_proxy: при False, будет вылетать эксепшн, если не указан прокси.
        :return:
        """
        # Должен получить коннекшн из очереди если нет то создать его
        if not self.__class__.driver_queue.empty():

            logger.debug("[{}] [{}] Reusing browser.".format(self.get_name(), self.get_task_id()))
            driver, self.driver_used, self.is_logged_in = self.__class__.driver_queue.get()

            self.driver = webdriver.Remote(command_executor=driver[0], desired_capabilities={})
            self.driver.session_id = driver[1]

            # Проверяем что браузер не упал
            if self.check_driver_alive():
                return self.driver
            else:
                self.driver, self.driver_used = None, 0
        if dedicated_proxy:
            proxy_use = self.get_proxy('check')
            proxy_host = self.get_proxy('host')
            proxy_port = self.get_proxy('port')
            proxy_is_socks = self.get_proxy('is_socks')
            proxy_socks_version = self.get_proxy('version')
            proxy_socks_remote_dns = getattr(self, 'proxy_remote_dns', True)
            proxy_has_auth = self.get_proxy('has_auth')
            proxy_cred = self.get_proxy('credentials')
        else:
            proxy_use = getattr(self, 'proxy_use', False)
            proxy_host = self.proxy_host
            proxy_port = int(self.proxy_port) if self.proxy_port is not None else None
            proxy_is_socks = getattr(self, 'proxy_is_socks', False)
            proxy_socks_version = int(getattr(self, 'proxy_socks_version', 5))
            proxy_socks_remote_dns = getattr(self, 'proxy_socks_remote_dns', True)
            proxy_has_auth = getattr(self, 'proxy_has_auth', False)

        fp = webdriver.FirefoxProfile()
        if proxy_has_auth and proxy_use and proxy_host is not None and proxy_port is not None:
            if dedicated_proxy:
                cred = proxy_cred
            else:
                cred = '{}:{}'.format(self.proxy_user, self.proxy_password)
            enc = base64.encodebytes(cred.encode(encoding='UTF-8')).decode().strip()
            fp.add_extension(os.path.join(API_ROOT_DIR, 'utils', 'modify_headers-0.7.1.1-fx.xpi'))
            fp.set_preference("modifyheaders.config.active", True)
            fp.set_preference("modifyheaders.config.alwaysOn", True)
            fp.set_preference("modifyheaders.headers.count", 1)
            fp.set_preference("modifyheaders.headers.action0", "Add")
            fp.set_preference("modifyheaders.headers.name0", "Proxy-Authorization")
            fp.set_preference("modifyheaders.headers.value0", "Basic {}".format(enc))
            fp.set_preference("modifyheaders.headers.enabled0", True)
            # TODO: Подразумевает, что все прокси с авторизацией не соксы, что для 5й версии сокса некорректно
            proxy_is_socks = False

        if self.browser_user_agent is not None:
            # так как теперь нет request, то и _id у него теперь нет, а есть 'request_task' - это айди API task
            logger.debug("[{}] [{}] : Setting custom user-agent: {}".format(self.get_name(),
                                                                           self.get_task_id(), self.browser_user_agent))
            fp.set_preference("general.useragent.override", self.browser_user_agent)

        if proxy_use and proxy_is_socks \
                and proxy_host is not None and proxy_port is not None:  # SOCKS прокси
            fp.set_preference("network.proxy.type", 1)
            fp.set_preference("network.proxy.socks", proxy_host)
            fp.set_preference("network.proxy.socks_port", int(proxy_port))
            fp.set_preference("network.proxy.socks_version", int(proxy_socks_version))
            fp.set_preference("network.proxy.socks_remote_dns", proxy_socks_remote_dns)
            logger.info("[{}] [{}] : SOCKS activated: {}:{}\nVersion: {}, Using remote DNS: {}".format(
                self.get_name(), self.get_task_id(), proxy_host, proxy_port,
                proxy_socks_version, proxy_socks_remote_dns
            ))
        elif proxy_use and \
                not proxy_is_socks \
                and proxy_host is not None and proxy_port is not None:  # НТТР проски
            fp.set_preference("network.proxy.type", 1)
            fp.set_preference("network.proxy.http", proxy_host)
            fp.set_preference("network.proxy.http_port", int(proxy_port))
            fp.set_preference("network.proxy.ftp", proxy_host)
            fp.set_preference("network.proxy.ftp_port", int(proxy_port))
            fp.set_preference("network.proxy.ssl", proxy_host)
            fp.set_preference("network.proxy.ssl_port", int(proxy_port))
            logger.info("[{}] [{}] : PROXY activated: {}:{}".format(self.get_name(),
                                                                    self.get_task_id(), proxy_host, proxy_port))

        else:
            if allow_no_proxy:
                logger.info("[{}] [{}] : Task without PROXY.".format(self.get_name(), self.get_task_id()))
                fp.set_preference("network.proxy.type", 0)  # Без прокси
            else:
                raise Exception("Start this grabber without proxy is not allowed!")

        fp.set_preference("browser.privatebrowsing.autostart", True)
        fp.set_preference("browser.startup.homepage_override.mstone", "ignore")
        fp.set_preference("startup.homepage_welcome_url.additional", "about:blank")
        # TODO: Возможность установки настроек браузера, передачей словаря с параметрами для него в аргумент функции.

        fp.update_preferences()

        self.driver = webdriver.Firefox(firefox_profile=fp)

        if self.driver:
            logger.debug("[{}] [{}] : Browser started".format(self.get_name(), self.get_task_id()))
        else:
            logger.error("[{}] [{}] : Browser isn't start".format(self.get_name(), self.get_task_id()))

        return self.driver

    def close_browser(self, force=False):
        # Если количество использований коннекшена меньше допустимого поместить его в очередь, иначе закрыть
        if self.driver_used + 1 < self.max_requests_in_per_user_connection and not force:
            # self.__class__.driver_queue.put([self.driver, self.driver_used + 1])
            # Экспериментальная возможность передачи данных о сессии
            self.__class__.driver_queue.put([
                [self.driver.command_executor._url, self.driver.session_id],
                self.driver_used + 1,
                self.is_logged_in
            ])
            self.__class__.driver_queue.task_done()
        else:
            try:
                self.driver.close()
            except:
                pass

    def get_proxy(self, value='check'):
        proxy_host = self._user_config.get('proxy_host')
        proxy_port = self._user_config.get('proxy_port')
        proxy_type = self._user_config.get('proxy_type', 'socks5')
        if value == 'check':
            if proxy_port is not None and len(proxy_port) > 0 \
                    and proxy_host is not None and len(proxy_host) > 0:
                return '{}://{}:{}'.format(proxy_type, proxy_host, proxy_port)
            else:
                return False
        elif value == 'host':
            return False if len(proxy_host) == 0 else proxy_host
        elif value == 'port':
            return False if len(proxy_port) == 0 else int(proxy_port)
        elif value == 'is_socks':
            return True if proxy_type.startswith('socks') else False
        elif value == 'short_type':
            return proxy_type[:-1] if proxy_type[-1:].isdigit() else proxy_type
        elif value == 'long_type':
            return proxy_type if len(proxy_type) else proxy_type
        elif value == 'version':
            return int(proxy_type[-1:]) if proxy_type[-1:].isdigit() else False
        elif value == 'has_auth':
            return len(self._user_config.get('proxy_login')) > 0 and len(self._user_config.get('proxy_password')) > 0
        elif value == 'credentials':
            return '{}:{}'.format(self._user_config.get('proxy_login'), self._user_config.get('proxy_password'))
        else:
            raise Exception("Invalid parameter")

    def release_user(self, db):
        user_stats = db.find_one({'_id': self._user_config.get('stats_object_id')})
        if user_stats:
            db.update({'_id': user_stats.get('_id')},
                      {'$set': {'locked': False}})

    def user_setup(self, db):
        if not os.path.exists(self.user_config_file):
            raise Exception('User config file not found!')
        self._user_config = {}
        config = ConfigParser()
        config.read(self.user_config_file)
        for section in config.sections():
            # Получить логин пользователя и проверить его доступность
            # Если пользователь доступен, то выбирается его секция для дальнейших действий
            # После того, как пользователь был выбран, необходимо поставить на него блокировку
            login = config.get(section, 'login')
            password = config.get(section, 'password')
            proxy_host = config.get(section, 'proxy_host')
            proxy_port = config.get(section, 'proxy_port')
            proxy_type = config.get(section, 'proxy_type')
            proxy_login = config.get(section, 'proxy_login')
            proxy_password = config.get(section, 'proxy_password')

            user_stats = db.find_one({'login': login})
            if user_stats and user_stats.get('locked', False):
                # Выбранный пользователь заблокирован(возможно используется в другом потоке), пропускаем
                continue

            self._user_config.update({'login': login, 'password': password, 'proxy_host': proxy_host,
                                      'proxy_port': proxy_port, 'proxy_type': proxy_type,
                                      'proxy_login': proxy_login, 'proxy_password': proxy_password})

            if user_stats:
                db.update({'_id': user_stats.get('_id')},
                          {'$set': {'locked': True}})
                self._user_config.update({'stats_object_id': user_stats.get('_id')})
            else:
                user_stats = db.insert({'login': login,
                                        'locked': True,
                                        'requests_count': 0,
                                        'last_req': datetime.datetime.now()
                                        })
                self._user_config.update({'stats_object_id': str(user_stats)})
            break

        if not self._user_config:
            raise Exception("No free users found!")
        return True

    def get_user_login(self):
        login = self._user_config.get('login')
        if login is None or len(login) == 0:
            raise Exception('Internal error. User login is None or empty!')
        return login

    def get_user_password(self):
        password = self._user_config.get('password')
        if password is None or len(password) == 0:
            raise Exception('Internal error. User password is None or empty!')
        return password

    def get_name(self):
        return self.__class__.__name__

    @staticmethod
    def shift(seq, n):
        n %= len(seq)
        return seq[n:] + seq[:n]

    def fill_users_queue(self):
        if not self.__class__.user_list_queue.empty() or \
                not os.path.exists(self.user_list_file):
            self.user_login, self.user_password = None, None
            return False

        with open(self.user_list_file, 'r', encoding="UTF-8") as f:
            for line in f:
                if line.startswith('#') or len(line.strip()) == 0:
                    continue
                self.__class__.user_list_queue.put([line.strip(), 0, None])
            if self.__class__.user_list_queue.empty():
                raise Exception("User list file '{}' not contain user info!".format(self.user_list_file))
        return True

    def get_user_from_queue(self):
        if self.__class__.user_list_queue.empty():
            self.user_login, self.user_password = None, None
            return False

        user, self.captcha_sequence, self.blocked_until = self.__class__.user_list_queue.get()
        start_user_login = user.split(self.user_list_file_login_password_separator)[0]
        # Крутим очередь пока не найдем нужного пользователя
        while True:
            if self.blocked_until is None or self.blocked_until < datetime.datetime.now():
                break
            else:
                self.__class__.user_list_queue.put([user, self.captcha_sequence, self.blocked_until])
                user, self.captcha_sequence, self.blocked_until = self.__class__.user_list_queue.get()
                # Если прошел круг пользователей и они все ещё блокированы, спим 5 минут
                if start_user_login == user.split(self.user_list_file_login_password_separator)[0]:
                    logger.info("{} All users is blocked. Sleep for 5 minutes...".format(self.get_task_id()))
                    time.sleep(5 * 60)
        self.user_login, self.user_password = user.split(self.user_list_file_login_password_separator)
        logger.info("{} User changed to {}.".format(self.get_task_id(), self.user_login))

    def return_user_to_queue(self):
        user = self.user_list_file_login_password_separator.join([self.user_login, self.user_password])
        if self.captcha_sequence >= 5:
            self.blocked_until = datetime.datetime.now() + datetime.timedelta(minutes=30)
        else:
            self.blocked_until = None
        self.__class__.user_list_queue.put([user, self.captcha_sequence, self.blocked_until])
